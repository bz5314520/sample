FROM openjdk:17

WORKDIR /usr/src/app

ARG JAR_FILE=target/*.jar

ADD ${JAR_FILE} /usr/src/app.jar

EXPOSE 9876

ENTRYPOINT java -jar /usr/src/app.jar

