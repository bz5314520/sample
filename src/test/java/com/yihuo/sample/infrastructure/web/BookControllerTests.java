package com.yihuo.sample.infrastructure.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yihuo.sample.application.book.BookApplicationService;
import com.yihuo.sample.domain.model.book.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(BookController.class)
class BookControllerTests {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    BookApplicationService bookApplicationService;

    ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void bookList() throws Exception {
        when(bookApplicationService.bookList())
                .thenReturn(List.of(new Book("1","动物世界")));

        mockMvc.perform(get("/books")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().json("""
                        [{"bookId":"1","name":"动物世界"}]
                        """));
    }

    @Test
    void addBook() throws Exception {
        mockMvc.perform(post("/books")
                .content("""
                        {"bookId":10,"name":1}
                        """)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}