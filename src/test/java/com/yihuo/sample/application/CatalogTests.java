package com.yihuo.sample.application;

import com.yihuo.sample.domain.model.book.Catalog;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class CatalogTests {

    @Test
    @DisplayName("新建根分类")
    void case1() {
        Catalog catalog = Catalog.rootCatalogValueOf("LS", "历史");
        Assertions.assertTrue(catalog.isRoot());
        Assertions.assertEquals(catalog.parentId(),Catalog.ROOT_ID);
    }

    @Test
    @DisplayName("子分类父级不能为空")
    void case2() {
        Assertions.assertThrows(RuntimeException.class,() -> new Catalog("HW","中文",null));
    }

}
