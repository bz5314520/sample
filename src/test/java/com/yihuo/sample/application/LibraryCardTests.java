package com.yihuo.sample.application;


import com.yihuo.sample.application.lending.LibraryCarApplicationService;
import com.yihuo.sample.application.lending.NewLibraryCardCommand;
import com.yihuo.sample.domain.model.lending.LendService;
import com.yihuo.sample.domain.model.lending.LibraryCardRepository;
import org.assertj.core.api.Condition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@DisplayName("借书卡用例")
@SpringBootTest(properties = "spring.profiles.active=test", webEnvironment = SpringBootTest.WebEnvironment.NONE)
class LibraryCardTests {

    @Autowired
    LibraryCarApplicationService libraryCarApplicationService;

    @Autowired
    LibraryCardRepository libraryCardRepository;

    @Test
    @DisplayName("同一用户只能拥有一张卡,不能重复制卡")
    void case1() {
        NewLibraryCardCommand newLibraryCardCommand = new NewLibraryCardCommand("1","达尔文","134118309593");

        libraryCarApplicationService.assignCard(newLibraryCardCommand);

        Assertions.assertThrows(RuntimeException.class,() -> libraryCarApplicationService.assignCard(newLibraryCardCommand));
    }

    @Test
    @DisplayName("添加一张卡")
    void case2() {
        NewLibraryCardCommand newLibraryCardCommand = new NewLibraryCardCommand("1","达尔文","134118309593");
        libraryCarApplicationService.assignCard(newLibraryCardCommand);
        org.assertj.core.api.Assertions.assertThat(libraryCardRepository.findByUserid("1"))
                .isPresent()
                .get()
                .is(new Condition<>(LendService::canLend,"新卡是可立即借书的"));
    }
}
