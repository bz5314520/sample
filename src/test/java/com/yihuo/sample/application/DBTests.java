package com.yihuo.sample.application;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.ThrowingSupplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;
import java.sql.Connection;

@DisplayName("校验数据库schema")
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@DataJpaTest
class DBTests {

    @TestConfiguration
    static class Config{
        @Bean
        public DataSource dataSource(){
            HikariConfig hikariConfig = new HikariConfig();
            hikariConfig.setJdbcUrl("jdbc:mysql://159.75.80.191:3306/book");
            hikariConfig.setUsername("root");
            hikariConfig.setPassword("root");
            hikariConfig.setDriverClassName("com.mysql.cj.jdbc.Driver");
            hikariConfig.setConnectionTimeout(5000);
            hikariConfig.validate();
            return new HikariDataSource(hikariConfig);
        }
    }

    @Test
    @DisplayName("获取连接")
    void case1(@Autowired DataSource dataSource) {
        Assertions.assertDoesNotThrow((ThrowingSupplier<Connection>) dataSource::getConnection);
    }

}
