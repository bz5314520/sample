package com.yihuo.sample.application;

import com.yihuo.sample.application.lending.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Set;


@Transactional
@DisplayName("还书用例")
@SpringBootTest(properties = "spring.profiles.active=test", webEnvironment = SpringBootTest.WebEnvironment.NONE)
class ReturningBookTests {

    @Autowired
    LibraryCarApplicationService libraryCarApplicationService;

    @Autowired
    LendingApplicationService lendingApplicationService;

    @Autowired
    ReturningApplicationService returningApplicationService;


    @Test
    @DisplayName("用户正常还书,再次借用")
    void case1() {
        NewLibraryCardCommand newLibraryCardCommand = new NewLibraryCardCommand("1","达尔文","134118309593");

        libraryCarApplicationService.assignCard(newLibraryCardCommand);

        LocalDate today = LocalDate.now();

        LendBookCommand lendBookCommand = new LendBookCommand("1","BK",today,today.plusDays(2));
        lendingApplicationService.lendBook(lendBookCommand);

        Set<ReturnBookInformation> returnBookInformation = Set.of(new ReturnBookInformation("BK", null));
        ReturningBookCommand returningBookCommand = new ReturningBookCommand(today.plusDays(1),returnBookInformation);

        returningApplicationService.returnBook(returningBookCommand);

        Assertions.assertDoesNotThrow(() -> lendingApplicationService.lendBook(new LendBookCommand("1","BK",LocalDate.now(),LocalDate.now().plusDays(2))));
    }

    @Test
    @DisplayName("借阅者逾期还书")
    void case2() {
        NewLibraryCardCommand newLibraryCardCommand = new NewLibraryCardCommand("1","达尔文","134118309593");

        libraryCarApplicationService.assignCard(newLibraryCardCommand);

        LocalDate today = LocalDate.now();

        LendBookCommand lendBookCommand = new LendBookCommand("1","BK",today,today.plusDays(2));

        lendingApplicationService.lendBook(lendBookCommand);

        Set<ReturnBookInformation> returnBookInformation = Set.of(new ReturnBookInformation("BK", null));
        ReturningBookCommand returningBookCommand = new ReturningBookCommand(today.plusDays(3),returnBookInformation);

        returningApplicationService.returnBook(returningBookCommand);

        Assertions.assertThrows(IllegalStateException.class,() -> lendingApplicationService.lendBook(lendBookCommand));
    }

    @Test
    @DisplayName("客户损坏了图书")
    void case3() {
        NewLibraryCardCommand newLibraryCardCommand = new NewLibraryCardCommand("1","达尔文","134118309593");
        libraryCarApplicationService.assignCard(newLibraryCardCommand);

        LocalDate today = LocalDate.now();
        LendBookCommand lendBookCommand = new LendBookCommand("1","BK",today,today.plusDays(2));
        lendingApplicationService.lendBook(lendBookCommand);

        Set<ReturnBookInformation> returnBookInformation = Set.of(new ReturnBookInformation("BK", "被损毁严重"));
        ReturningBookCommand returningBookCommand = new ReturningBookCommand(today,returnBookInformation);
        returningApplicationService.returnBook(returningBookCommand);

        Assertions.assertThrows(IllegalStateException.class,() -> lendingApplicationService.lendBook(lendBookCommand));
    }

}
