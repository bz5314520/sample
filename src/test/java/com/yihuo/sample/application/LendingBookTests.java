package com.yihuo.sample.application;

import com.yihuo.sample.application.book.BookApplicationService;
import com.yihuo.sample.application.lending.LendBookCommand;
import com.yihuo.sample.application.lending.LendingApplicationService;
import com.yihuo.sample.application.lending.LibraryCarApplicationService;
import com.yihuo.sample.application.lending.NewLibraryCardCommand;
import com.yihuo.sample.domain.model.book.Book;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;


@Transactional
@DisplayName("借书用例")
@SpringBootTest(properties = "spring.profiles.active=test", webEnvironment = SpringBootTest.WebEnvironment.NONE)
class LendingBookTests {

    @Autowired
    LibraryCarApplicationService libraryCarApplicationService;

    @Autowired
    LendingApplicationService lendingApplicationService;

    @Autowired
    BookApplicationService bookApplicationService;

    @BeforeEach
    void given(){
        NewLibraryCardCommand newLibraryCardCommand = new NewLibraryCardCommand("1","达尔文","134118309593");
        libraryCarApplicationService.assignCard(newLibraryCardCommand);
    }

    @Test
    @DisplayName("吊销借书卡不能借书")
    void case1() {

        libraryCarApplicationService.revoke("1");

        LendBookCommand lendBookCommand = new LendBookCommand("1","BK",LocalDate.now(),LocalDate.now().plusDays(2));

        Assertions.assertThrows(IllegalStateException.class, () -> lendingApplicationService.lendBook(lendBookCommand));
    }

    @Test
    @DisplayName("盗版借书卡不能借书")
    void case2() {
        LendBookCommand lendBookCommand = new LendBookCommand("N/A","BK",LocalDate.now(),LocalDate.now().plusDays(2));

        Assertions.assertThrows(IllegalStateException.class, () -> lendingApplicationService.lendBook(lendBookCommand));
    }

    @Test
    @DisplayName("正常借书")
    void case3() {
        LendBookCommand lendBookCommand = new LendBookCommand("1","BK",LocalDate.now(),LocalDate.now().plusDays(2));

        lendingApplicationService.lendBook(lendBookCommand);
        Assertions.assertDoesNotThrow(() -> lendingApplicationService.lendBook(lendBookCommand));
    }

    @Test
    @DisplayName("借出的书应从展示书单中删除")
    void case4() {
        Book book1 = new Book("1", "天工开物");
        Book book2 = new Book("2", "奇门遁甲");
        bookApplicationService.add(book1);
        bookApplicationService.add(book2);

        // 借走book2
        LendBookCommand lendBookCommand = new LendBookCommand("1",book2.bookId(),LocalDate.now(),LocalDate.now().plusDays(2));

        lendingApplicationService.lendBook(lendBookCommand);

        Assertions.assertFalse(bookApplicationService.bookList().contains(book2));
    }


}
