package com.yihuo.sample.domain.model.lending;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("借书卡")
class LibraryCardTests {

    @Test
    @DisplayName("新卡用户id不能为空")
    void case1_() {
        assertThrows(RuntimeException.class, () -> new LibraryCard("", "包子", "12345678901"));
    }

    @Test
    @DisplayName("新卡默认状态为激活积分10")
    void case2_() {
        LibraryCard card = new LibraryCard("1", "包子", "12345678901");
        assertTrue(card.isActive());
        assertEquals(card.point().value(), 10);
    }

    @Test
    @DisplayName("注销后卡片为禁用状态")
    void case3_() {
        LibraryCard card = new LibraryCard("1", "包子", "12345678901");
        card.revoke();
        assertFalse(card.isActive());
    }

    @Test
    @DisplayName("给借书卡加分")
    void case4() {
        LibraryCard card = new LibraryCard("1", "包子", "12345678901");
        int rawValue = card.point().value();
        card.point().plus(10);
        assertEquals(card.point().value(), rawValue + 10);
    }

    @Test
    @DisplayName("给借书卡减分")
    void case5() {
        LibraryCard card = new LibraryCard("1", "包子", "12345678901");
        int rawValue = card.point().value();
        card.point().minus(10);
        assertEquals(card.point().value(), rawValue - 10);
    }
}