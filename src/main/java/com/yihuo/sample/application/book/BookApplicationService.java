package com.yihuo.sample.application.book;

import com.yihuo.sample.domain.model.book.Book;
import com.yihuo.sample.domain.model.book.BookRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookApplicationService {

    private final BookRepository bookRepository;

    public BookApplicationService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public List<Book> bookList(){
        return bookRepository.findAll();
    }

    public void add(Book book){
         bookRepository.add(book);
    }
}
