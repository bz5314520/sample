package com.yihuo.sample.application.lending;

public record ReturnBookInformation(String bookId,String damageDescribe) {
}
