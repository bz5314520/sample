package com.yihuo.sample.application.lending;

import com.yihuo.sample.domain.model.lending.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ReturningApplicationService {

    private final LendLogRepository lendLogRepository;

    private final BookDamageRepository bookDamageRepository;

    private final LibraryCardRepository libraryCardRepository;

    public ReturningApplicationService(LendLogRepository lendLogRepository, BookDamageRepository bookDamageRepository, LibraryCardRepository libraryCardRepository) {
        this.lendLogRepository = lendLogRepository;
        this.bookDamageRepository = bookDamageRepository;
        this.libraryCardRepository = libraryCardRepository;
    }

    @Transactional
    public void returnBook(ReturningBookCommand command){
        command.returnBookInformation()
                .forEach(returnBookInformation -> lendLogRepository.findByBookIdAndCompleteIsFalse(returnBookInformation.bookId())
                        .ifPresent(lendLog -> {
                            lendLog.complete();
                            if (returnBookInformation.damageDescribe() != null) {
                                bookDamageRepository.add(new BookDamage(lendLog.userid(), returnBookInformation.bookId(), returnBookInformation.damageDescribe()));
                                libraryCardRepository.findByUserid(lendLog.userid())
                                        .ifPresent(i -> i.point().minus(5));
                            }
                            if(lendLog.isExpire(command.dateOfReturn())){
                                libraryCardRepository.findByUserid(lendLog.userid())
                                        .ifPresent(libraryCard -> libraryCard.point().minus(5));
                            }
                        }));
    }

}
