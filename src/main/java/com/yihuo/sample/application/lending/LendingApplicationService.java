package com.yihuo.sample.application.lending;

import com.yihuo.sample.domain.model.book.BookRepository;
import com.yihuo.sample.domain.model.lending.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LendingApplicationService {

    private final LibraryCardRepository libraryCardRepository;

    private final LendLogRepository lendLogRepository;

    private final BookRepository bookRepository;

    public LendingApplicationService(LibraryCardRepository libraryCardRepository, LendLogRepository lendLogRepository, BookRepository bookRepository, LendService lendService) {
        this.libraryCardRepository = libraryCardRepository;
        this.lendLogRepository = lendLogRepository;
        this.bookRepository = bookRepository;
    }

    @Transactional
    public void lendBook(LendBookCommand command){
        LibraryCard card = libraryCardRepository.findByUserid(command.userid()).orElseThrow(() -> new IllegalStateException("系统没有该卡记录"));
        if(!LendService.canLend(card)) throw new IllegalStateException("信用不够");
        bookRepository.removeByBookId(command.bookId());
        this.lendLogRepository.save(new LendLog(command.userid(),command.bookId(),command.start(), command.deadline()));
    }

}
