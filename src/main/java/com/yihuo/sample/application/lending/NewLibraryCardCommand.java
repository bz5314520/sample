package com.yihuo.sample.application.lending;

public record NewLibraryCardCommand(String userid,String username,String phone) { }
