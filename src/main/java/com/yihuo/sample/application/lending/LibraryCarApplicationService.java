package com.yihuo.sample.application.lending;

import com.yihuo.sample.domain.model.lending.LibraryCard;
import com.yihuo.sample.domain.model.lending.LibraryCardRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LibraryCarApplicationService {

    private final LibraryCardRepository libraryCardRepository;

    public LibraryCarApplicationService(LibraryCardRepository libraryCardRepository) {
        this.libraryCardRepository = libraryCardRepository;
    }

    @Transactional
    public void assignCard(NewLibraryCardCommand command){
        libraryCardRepository.add(new LibraryCard(command.userid(), command.username(), command.phone()));
    }

    @Transactional
    public void revoke(String userid){
        libraryCardRepository.findByUserid(userid)
                                .ifPresent(LibraryCard::revoke);
    }


}
