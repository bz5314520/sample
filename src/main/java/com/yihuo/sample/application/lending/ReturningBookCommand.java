package com.yihuo.sample.application.lending;

import java.time.LocalDate;
import java.util.Set;

public record ReturningBookCommand(LocalDate dateOfReturn,
                                   Set<ReturnBookInformation> returnBookInformation) {
}
