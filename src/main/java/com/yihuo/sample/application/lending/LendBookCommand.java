package com.yihuo.sample.application.lending;

import java.time.LocalDate;

public record LendBookCommand(String userid,String bookId,LocalDate start,LocalDate deadline) {
}
