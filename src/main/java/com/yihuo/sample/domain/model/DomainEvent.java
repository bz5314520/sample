package com.yihuo.sample.domain.model;

import java.time.Instant;

public interface DomainEvent {
    Instant occurredOn();
}