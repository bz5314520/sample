package com.yihuo.sample.domain.model.lending;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface LendLogRepository extends CrudRepository<LendLog,Long> {

    List<LendLog> findAllByUseridOrderByStartDesc(String userid);

    Optional<LendLog> findByBookIdAndCompleteIsFalse(String bookId);
}
