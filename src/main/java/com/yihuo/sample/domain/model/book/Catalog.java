package com.yihuo.sample.domain.model.book;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Catalog {
    public static final String ROOT_ID = "ROOT";
    @Id
    private String id;
    private String name;
    private String parentId;
    private boolean isRoot;

    protected Catalog(){}

    private Catalog(String id,String name,String parentId, boolean isRoot){
        Objects.requireNonNull(parentId,"父级ID是必须的");
        this.id = id;
        this.name = name;
        this.parentId = parentId;
        this.isRoot = isRoot;
    }

    public Catalog(String id, String name, String parentId) {
        this(id,name,parentId,false);
    }

    public static Catalog rootCatalogValueOf(String id,String name){
        return new Catalog(id,name,ROOT_ID,true);
    }

    public boolean isRoot(){
        return this.isRoot;
    }

    public String id() {
        return id;
    }

    public String name() {
        return name;
    }

    public String parentId() {
        return parentId;
    }
}
