package com.yihuo.sample.domain.model.lending;

import javax.persistence.*;

@Entity
public class LibraryCard {

    @Id
    private String userid;
    private String username;
    private String phone;
    private boolean isActive = true;
    @Embedded
    private final CreditPoint point = new CreditPoint(10);

    protected LibraryCard() {
    }

    public LibraryCard(String userid, String username, String phone) {
        if(userid == null || userid.isBlank()) throw new IllegalArgumentException("userid is blank");
        this.userid = userid;
        this.username = username;
        this.phone = phone;
    }


    public void disable(){
        this.isActive = false;
    }

    public void enable(){
        this.isActive = true;
    }

    public void revoke(){
        this.isActive = false;
    }

    public String userid() {
        return this.userid;
    }

    public String username() {
        return this.username;
    }

    public String phone() {
        return this.phone;
    }

    public boolean isActive() {
        return this.isActive;
    }

    public CreditPoint point() {
        return this.point;
    }
}
