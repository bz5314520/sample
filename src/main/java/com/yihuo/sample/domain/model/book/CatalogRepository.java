package com.yihuo.sample.domain.model.book;

import java.util.List;

public interface CatalogRepository {

    void add(Catalog catalog);

    List<Catalog> findAll();

}
