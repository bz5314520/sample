package com.yihuo.sample.domain.model.book;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import javax.persistence.Entity;
import javax.persistence.Id;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@Entity
public class Book {

    @Id
    private String bookId;

    private String name;

    public Book(String bookId, String name) {
        this.bookId = bookId;
        this.name = name;
    }

    protected Book() {

    }

    public String bookId() {
        return bookId;
    }

    public String name() {
        return name;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookId='" + bookId + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
