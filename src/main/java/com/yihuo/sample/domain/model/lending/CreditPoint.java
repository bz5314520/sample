package com.yihuo.sample.domain.model.lending;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CreditPoint {

    @Column(name = "credit_point")
    private int value;

    CreditPoint(int value) {
        this.value = value;
    }

    protected CreditPoint() {
    }

    public void minus(int amountToSubtract){
        this.value -= amountToSubtract;
    }

    public void plus(int amountToAdd){ this.value += amountToAdd; }

    public int value() {
        return this.value;
    }
}
