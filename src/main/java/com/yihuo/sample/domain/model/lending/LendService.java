package com.yihuo.sample.domain.model.lending;

import org.springframework.stereotype.Service;

@Service
public class LendService {

    public static boolean canLend(LibraryCard libraryCard){
        return libraryCard.isActive() && libraryCard.point().value() > 6;
    }

}
