package com.yihuo.sample.domain.model.lending;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface BookDamageRepository {

    void add(BookDamage bookDamage);

    Page<BookDamage> pagingBookDamage(PageRequest pageRequest);

    List<BookDamage> findBookDamageTracking(String book);

}
