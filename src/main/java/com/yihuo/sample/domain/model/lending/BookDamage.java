package com.yihuo.sample.domain.model.lending;

import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class BookDamage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String bookId;

    @Lob
    private String content;

    private String userid;

    @CreatedDate
    private LocalDate createdDate;

    protected BookDamage(){}

    public BookDamage(String bookId,String userid, String content) {
        this.bookId = bookId;
        this.content = content;
        this.userid = userid;
    }

    public Long id() {
        return id;
    }

    public String bookId() {
        return bookId;
    }

    public String content() {
        return content;
    }

    public LocalDate createdDate() {
        return createdDate;
    }

    public String userid() {
        return this.userid;
    }
}
