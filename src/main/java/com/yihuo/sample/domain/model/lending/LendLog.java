package com.yihuo.sample.domain.model.lending;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class LendLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String userid;
    private LocalDate start;
    private LocalDate deadline;
    private String bookId;
    private boolean complete = false;

    protected LendLog(){
    }

    public LendLog(String userid, String bookId,LocalDate start, LocalDate deadline) {
        this.userid = userid;
        this.bookId = bookId;
        if(deadline.isBefore(start)) throw new IllegalStateException("不合法的时间设置");
        this.deadline = deadline;
        this.start = start;
    }

    public Long id() {
        return this.id;
    }

    public String userid() {
        return this.userid;
    }

    public LocalDate start() {
        return this.start;
    }

    public LocalDate deadline() {
        return this.deadline;
    }

    public String bookId() {
        return this.bookId;
    }

    public boolean isComplete() {
        return this.complete;
    }

    public boolean isExpire(LocalDate dateToCompare){
        return this.deadline.isBefore(dateToCompare);
    }

    public void complete() {
        this.complete = true;
    }
}
