package com.yihuo.sample.domain.model.lending;

import java.util.List;
import java.util.Optional;

public interface LibraryCardRepository {

    void add(LibraryCard libraryCard);

    Optional<LibraryCard> findByUserid(String userid);

    List<LibraryCard> findAll();
}
