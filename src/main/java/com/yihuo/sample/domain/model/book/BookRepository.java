package com.yihuo.sample.domain.model.book;

import java.util.List;
import java.util.Optional;

public interface BookRepository {

        void add(Book book);

        Optional<Book> bookOfId(String bookId);

        void removeByBookId(String bookId);

        List<Book> findAll();
}
