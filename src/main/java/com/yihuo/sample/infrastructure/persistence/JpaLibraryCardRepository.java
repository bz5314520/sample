package com.yihuo.sample.infrastructure.persistence;

import com.yihuo.sample.domain.model.lending.LibraryCard;
import com.yihuo.sample.domain.model.lending.LibraryCardRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

@Repository
public class JpaLibraryCardRepository implements LibraryCardRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void add(LibraryCard libraryCard) {
        entityManager.persist(libraryCard);
    }

    @Override
    public Optional<LibraryCard> findByUserid(String userid){
        return Optional.ofNullable(entityManager.find(LibraryCard.class,userid));
    }

    @Override
    public List<LibraryCard> findAll(){
        return entityManager.createQuery("select LibraryCard from LibraryCard",LibraryCard.class).getResultList();
    }
}
