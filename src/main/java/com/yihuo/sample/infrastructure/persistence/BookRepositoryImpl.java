package com.yihuo.sample.infrastructure.persistence;

import com.yihuo.sample.domain.model.book.Book;
import com.yihuo.sample.domain.model.book.BookRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface BookRepositoryImpl extends BookRepository, CrudRepository<Book,String> {
    @Override
    default void add(Book book) {
        this.save(book);
    }

    @Override
    default Optional<Book> bookOfId(String bookId) {
        return this.findById(bookId);
    }

    @Override
    void removeByBookId(String bookId);

    @Override
    List<Book> findAll();
}
