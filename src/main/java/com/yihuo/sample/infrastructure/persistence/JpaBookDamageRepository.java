package com.yihuo.sample.infrastructure.persistence;

import com.yihuo.sample.domain.model.lending.BookDamage;
import com.yihuo.sample.domain.model.lending.BookDamageRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;
import java.util.List;

@Repository
public class JpaBookDamageRepository implements BookDamageRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void add(BookDamage bookDamage) {
        em.persist(bookDamage);
    }

    @Override
    public Page<BookDamage> pagingBookDamage(PageRequest pageRequest) {
        List<BookDamage> results = em.createQuery("select b from BookDamage b", BookDamage.class)
                .setMaxResults(pageRequest.getPageSize())
                .setFirstResult((pageRequest.getPageNumber() - 1) * pageRequest.getPageSize())
                .setMaxResults(pageRequest.getPageSize())
                .getResultList();
        return PageableExecutionUtils.getPage(results,pageRequest, () -> em.createQuery("select count(b) from BookDamage b", BigInteger.class).getSingleResult().longValue());
    }


    @Override
    public List<BookDamage> findBookDamageTracking(String bookId) {
        return em.createQuery("select BookDamage from BookDamage where bookId = :bookId order by createdDate desc ",BookDamage.class)
                    .setParameter("bookId",bookId)
                    .getResultList();
    }
}
