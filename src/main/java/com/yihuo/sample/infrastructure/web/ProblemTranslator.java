package com.yihuo.sample.infrastructure.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ProblemTranslator extends ResponseEntityExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Problem> handle(IllegalArgumentException ex, HttpServletRequest httpServletRequest){
       return ResponseEntity.badRequest().body(new Problem("参数错误",ex.getMessage(), HttpStatus.BAD_REQUEST.value(), httpServletRequest.getRequestURL().toString()));
    }

}
