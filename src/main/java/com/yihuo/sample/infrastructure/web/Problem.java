package com.yihuo.sample.infrastructure.web;


/**
 *
 * HTTP ERROR RESPONSE WITH 4XX OR 5XX
 * @see <a href="https://datatracker.ietf.org/doc/html/rfc7807">rfc 7807</a>
 */
public class Problem {
    /**
     *  错误信息详情描述文档地址start with http or https,如果为空 about:blank
     */
    private String type;
    /**
     *  错误简短的描述 ,人类可读
     */
    private String title;
    /**
     *  错误详细描述 ,人类可读
     */
    private String detail;

    /**
     *  http 状态码
     */
    private int status;

    /**
     * 错误的请求实例链接
     */
    private String instance;

    public Problem(String type, String title, String detail, int status, String instance) {
        this.type = type;
        this.title = title;
        this.detail = detail;
        this.status = status;
        this.instance = instance;
    }

    public Problem(String title, String detail, int status, String instance) {
        this.type = "about:blank";
        this.title = title;
        this.detail = detail;
        this.status = status;
        this.instance = instance;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }
}
