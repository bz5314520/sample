package com.yihuo.sample.infrastructure.web;

import com.yihuo.sample.application.book.BookApplicationService;
import com.yihuo.sample.domain.model.book.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
public class BookController {

    private BookApplicationService bookApplicationService;

    @GetMapping("/books")
    public ResponseEntity<List<Book>> bookList(){
        return ResponseEntity.ok(bookApplicationService.bookList());
    }

    @PostMapping("/books")
    public ResponseEntity<Void> addBook(@RequestBody Book book){
        bookApplicationService.add(book);
        return ResponseEntity.ok().build();
    }

    @Autowired
    public void setBookApplicationService(BookApplicationService bookApplicationService) {
        this.bookApplicationService = bookApplicationService;
    }


}
