package com.yihuo.sample.infrastructure;

import java.io.IOException;
import java.io.OutputStream;

public interface PdfPrinter {

    void write(String template, OutputStream os) throws IOException;

}
