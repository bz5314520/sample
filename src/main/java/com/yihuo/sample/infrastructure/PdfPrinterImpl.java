package com.yihuo.sample.infrastructure;

import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

public class PdfPrinterImpl implements PdfPrinter {

    @Override
    public void write(String template, OutputStream os) throws IOException {
        new PdfRendererBuilder().useFont(new File("arialuni.ttf"), "arial")
                .withHtmlContent(template, null)
                .toStream(os)
                .run();
    }

}
