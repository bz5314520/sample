package com.yihuo.sample.infrastructure;

import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.springframework.util.FileSystemUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PrinterApplicationService {

    private PdfPrinter pdfPrinter;

    private ExecutorService executorService = Executors.newSingleThreadExecutor();


    public void render(List<Map<String, ?>> models, String template, boolean ignoreError, OutputStream os) throws IOException {
        PDFMergerUtility utility = new PDFMergerUtility();
        File workDir = newTempWorkDir();
        models.stream().parallel()
                .map(model -> {
                    try {
                        String id = workDir.getPath() + File.separator + UUID.randomUUID() + ".pdf";
                        pdfPrinter.write(template, new FileOutputStream(id));
                        return id;
                    } catch (IOException e) {
                        if (!ignoreError) throw new RuntimeException("处理失败");
                    }
                    return null;
                }).filter(Objects::nonNull)
                .map(name -> {
                    try {
                        return new FileInputStream(name);
                    } catch (FileNotFoundException e) {
                        // 这还错误就不管了
                        throw new RuntimeException(e);
                    }
                })
                .forEach(utility::addSource);
        utility.setDestinationStream(os);
        utility.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly());
        executorService.execute(() -> FileSystemUtils.deleteRecursively(workDir));
    }

    public File newTempWorkDir() throws IOException {
        return Files.createTempDirectory(Paths.get(System.getProperty("user.home")), "pdf").toFile();
    }

}
