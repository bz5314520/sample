### 集成代码前请在命令行输入并运行  mvn clean verify !!! 重要的事情说三遍 测试结果很重要 !!! 测试结果很重要 !!! 测试结果很重要 !!!

### Java Spring template project
[![pipeline status](https://gitlab.com/bz5314520/sample/badges/master/pipeline.svg)](https://gitlab.com/bz5314520/sample/-/commits/master)
[![coverage report](https://gitlab.com/bz5314520/sample/badges/master/coverage.svg)](https://gitlab.com/bz5314520/sample/-/commits/master)

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.